package ru.tsc.tambovtsev.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.Task;

public interface ITaskGraphRepository extends IOwnerGraphRepository<Task> {

    void deleteByUserIdAndProjectId(@Nullable String userId, @Nullable String projectId);

}
