package ru.tsc.tambovtsev.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.dto.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.service.dto.ITaskService;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.field.*;

import java.util.*;

@Service
public class TaskService extends AbstractUserOwnedService<TaskDTO, ITaskRepository> implements ITaskService {

    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Override
    public ITaskRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        final TaskDTO task = repository.findByUserIdAndId(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        task.setUserId(userId);
        repository.save(task);
        return task;
    }

}
