package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;

public interface IAuthService {

    void login(@Nullable String login, @Nullable String password);

    void logout();

    @Nullable
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO getUser();

    @NotNull
    UserDTO check(@Nullable String login, @Nullable String password);

    @NotNull
    String getUserId();

    boolean isAuth();

    void checkRoles(@Nullable Role[] roles);

}

