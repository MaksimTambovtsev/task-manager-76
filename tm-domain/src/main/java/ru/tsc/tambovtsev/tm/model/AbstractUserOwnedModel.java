package ru.tsc.tambovtsev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnedModel extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;

}

