package ru.tsc.tambovtsev.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.tsc.tambovtsev.tm.marker.IntegrationCategory;
import ru.tsc.tambovtsev.tm.model.Result;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.util.UserUtil;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Category(IntegrationCategory.class)
public class TaskRestEndpointTest {

    @Nullable
    private static String sessionId;

    @NotNull
    private static final String TASK_URL = "http://localhost:8080/api/task/";

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    @NotNull
    private final Task task = new Task("Task1", "Description1");

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?login=test&password=test";
        header.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final ResponseEntity<Result> response =
                restTemplate.postForEntity(url, new HttpEntity<>(""), Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isValue());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        @NotNull final List<HttpCookie> cookies = HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
    }

    private static <T> ResponseEntity<T> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity,
            @NotNull final Class<T> responseType
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, responseType);
    }

    @Before
    public void initTest() {
        @NotNull final String url = TASK_URL + "save/";
        task.setUserId(UserUtil.getUserId());
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task, header), Task.class);
    }

    @After
    public void clean() {
        @NotNull final String url = TASK_URL + "clear";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header), Task.class);
    }

    @AfterClass
    public static void logout() {
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(header), Result.class);
    }

    private long count() {
        @NotNull final String logoutUrl = TASK_URL + "count";
        @NotNull final ResponseEntity<Long> response =
                sendRequest(logoutUrl, HttpMethod.GET, new HttpEntity<>(header), Long.class);
        if (response.getStatusCode() != HttpStatus.OK) return 0;
        @Nullable Long result = response.getBody();
        if (result == null) return 0;
        return result;
    }

    @Test
    public void findAllTest() {
        @NotNull final String url = TASK_URL + "findAll/";
        @NotNull final ResponseEntity<List> response =
                sendRequest(url, HttpMethod.GET, new HttpEntity<>(header), List.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable final List taskList = response.getBody();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(1, taskList.size());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String url = TASK_URL + "findById/" + task.getId();
        @NotNull final ResponseEntity<Task> response =
                sendRequest(url, HttpMethod.GET, new HttpEntity<>(header), Task.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable final Task taskFind = response.getBody();
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task.getId(), taskFind.getId());
    }

    @Test
    public void saveTest() {
        @NotNull final String url = TASK_URL + "save/";
        @NotNull final Task taskNew = new Task("Task2", "Description2");
        @NotNull final ResponseEntity<Task> response =
                sendRequest(url, HttpMethod.POST, new HttpEntity<>(taskNew, header), Task.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable final Task taskResult = response.getBody();
        Assert.assertNotNull(taskResult);
        Assert.assertEquals(taskResult.getId(), taskNew.getId());
    }

    @Test
    public void deleteTest() {
        @NotNull final String url = TASK_URL + "delete/";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(task, header), Task.class);
        Assert.assertEquals(0, count());
    }

    @Test
    public void deleteAllTest() {
        @NotNull final String url = TASK_URL + "deleteAll/";
        sendRequest(
                url,
                HttpMethod.DELETE,
                new HttpEntity<>(Collections.singletonList(task), header),
                Task.class
        );
        Assert.assertEquals(0, count());
    }

    @Test
    public void clearTest() {
        @NotNull final String url = TASK_URL + "clear";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header), Task.class);
        Assert.assertEquals(0, count());
    }

    @Test
    public void deleteByIdTest() {
        @NotNull final String url = TASK_URL + "deleteById/" + task.getId();
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header), Task.class);
        Assert.assertEquals(0, count());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(1, count());
    }
    
}
