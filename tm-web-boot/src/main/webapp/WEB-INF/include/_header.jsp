<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
    <head>
        <title>TASK MANAGER</title>
    </head>
    <style>
        h1 {
            font-size: 1.6em;
        }

        a {
            color: darkblue;
        }
    </style>

<body>
    <table width="100%" height="100%" border="1" style="padding: 10px;">
        <tr>
            <td>
                <table width="100%" border="0">
                    <tr>
                        <td height="35" width="100%" align="left">
                              <a href="/"><b>TASK MANAGER</b></a>
                        </td>
            <td width="400" nowrap="nowrap" align="right">
                <sec:authorize access="isAuthenticated()">
                    <a href="/projects">PROJECTS</a>

                    <a href="/tasks"> TASKS</a>

                     USER: <sec:authentication property="name"/>

                     <a href="/logout">LOGOUT</a>
                </sec:authorize>
                <sec:authorize access="!isAuthenticated()">
                     <a href="/login">LOGIN</a>
                </sec:authorize>

            </td>
        </tr>
        </table>
        </td>
        </tr>
        <tr>
            <td height="100%" valign="top" style="padding: 10px;">