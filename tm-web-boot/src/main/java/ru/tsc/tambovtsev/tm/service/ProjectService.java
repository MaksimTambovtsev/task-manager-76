package ru.tsc.tambovtsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.service.IProjectService;
import ru.tsc.tambovtsev.tm.exception.AccessException;
import ru.tsc.tambovtsev.tm.model.Project;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @NotNull
    @Override
    public Project create(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        @NotNull final Project project = new Project(
                "New Project: " + System.currentTimeMillis(),
                "Description",
                new Date(),
                new Date());
        project.setUserId(userId);
        save(project);
        return project;
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        @NotNull final Project project = new Project(
                name,
                "Description",
                new Date(),
                new Date());
        project.setUserId(userId);
        save(project);
        return project;
    }

    @NotNull
    @Override
    public Project save(@NotNull final Project project) {
        return repository.save(project);
    }

    @Nullable
    @Override
    public Collection<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        return repository.findByUserIdAndId(userId, id);
    }

    @Override
    public void removeById(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public void remove(@NotNull final Project project) {
        repository.delete(project);
    }

    @Override
    public void remove(@NotNull final List<Project> projects) {
        projects
                .stream()
                .forEach(this::remove);
    }

    @Override
    public void removeByUserId(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public boolean existsByUserIdAndId(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        repository.deleteAllByUserId(userId);
    }

    @Override
    public long count(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        return repository.countByUserId(userId);
    }

}
