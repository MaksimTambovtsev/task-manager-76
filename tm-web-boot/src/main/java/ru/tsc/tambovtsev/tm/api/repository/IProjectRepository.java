package ru.tsc.tambovtsev.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.tambovtsev.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends JpaRepository<Project, String> {

    Project findByUserIdAndId(String userId, String id);

    long countByUserId(String userId);

    List<Project> findAllByUserId(String userId);

    void deleteByUserIdAndId(String userId, String id);

    void deleteAllByUserId(String userId);

    boolean existsByUserIdAndId(String userId, String id);

}
