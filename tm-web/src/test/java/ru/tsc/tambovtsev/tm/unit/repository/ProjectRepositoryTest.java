package ru.tsc.tambovtsev.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.config.DatabaseConfiguration;
import ru.tsc.tambovtsev.tm.marker.UnitCategory;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.util.UserUtil;

import java.util.List;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class ProjectRepositoryTest {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @NotNull
    @Autowired
    private AuthenticationManager manager;

    @NotNull
    private final Project project1 = new Project("Project1");

    @NotNull
    private final Project project2 = new Project("Project2");

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = manager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        repository.save(project1);
        repository.save(project2);
    }

    @After
    public void clean() {
        repository.deleteAll();
    }

    @Test
    public void findAllTest() {
        @Nullable final List<Project> projectList = repository.findAll();
        Assert.assertEquals(2, projectList.size());
    }

    @Test
    public void findAllByUserIdTest() {
        @Nullable final List<Project> projectList = repository.findAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(2, projectList.size());
    }

    @Test
    public void findByUserIdAndIdTest() {
        @Nullable final Project project = repository.findByUserIdAndId(UserUtil.getUserId(), project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), project1.getId());
    }

    @Test
    public void deleteByUserIdTest() {
        repository.deleteByUserIdAndId(project1.getUserId(), project1.getId());
        Assert.assertEquals(1, repository.countByUserId(UserUtil.getUserId()));
    }

    @Test
    public void deleteByIdTest() {
        repository.deleteById(project2.getId());
        Assert.assertEquals(1, repository.countByUserId(UserUtil.getUserId()));
    }

    @Test
    public void deleteAllByUserIdTest() {
        repository.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, repository.countByUserId(UserUtil.getUserId()));
    }

}
