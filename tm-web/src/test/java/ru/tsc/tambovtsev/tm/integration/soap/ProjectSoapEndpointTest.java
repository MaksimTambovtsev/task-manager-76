package ru.tsc.tambovtsev.tm.integration.soap;

import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.tsc.tambovtsev.tm.api.endpoint.IAuthEndpointImpl;
import ru.tsc.tambovtsev.tm.api.endpoint.IProjectEndpointImpl;
import ru.tsc.tambovtsev.tm.client.AuthSoapEndpointClient;
import ru.tsc.tambovtsev.tm.client.ProjectSoapEndpointClient;
import ru.tsc.tambovtsev.tm.marker.IntegrationCategory;
import ru.tsc.tambovtsev.tm.model.Project;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Category(IntegrationCategory.class)
public class ProjectSoapEndpointTest {

    @NotNull
    private static IAuthEndpointImpl authEndpoint;

    @NotNull
    private static IProjectEndpointImpl projectEndpoint;

    @Nullable
    private static String sessionId = null;

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private final Project project = new Project("Project1", "Description1");

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login("test", "test").isValue());
        projectEndpoint = ProjectSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) projectEndpoint;
        @NotNull Map<String, List<String>> headers =
                CastUtils.cast(
                        (Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS)
                );
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @AfterClass
    public static void logout() {
        authEndpoint.logout();
    }

    @Before
    public void initTest() {
        projectEndpoint.save(project);
    }

    @After
    @SneakyThrows
    public void clean() {
        projectEndpoint.clear();
    }

    @Test
    public void findAllTest() {
        @Nullable final List<Project> projects = projectEndpoint.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findByIdTest() {
        @Nullable final Project project2 = projectEndpoint.findById(project.getId());
        Assert.assertNotNull(project2);
        Assert.assertEquals(project2.getId(), project.getId());
    }

    @Test
    public void saveTest() {
        @NotNull final Project project2 = new Project("Project2", "Description2");
        @NotNull final Project projectRes = projectEndpoint.save(project2);
        Assert.assertNotNull(projectRes);
        Assert.assertEquals(projectRes.getId(), project2.getId());
    }

    @Test
    public void deleteTest() {
        projectEndpoint.delete(project);
        Assert.assertEquals(0, projectEndpoint.count());
    }

    @Test
    public void deleteAllTest() {
        projectEndpoint.deleteAll(Collections.singletonList(project));
        Assert.assertEquals(0, projectEndpoint.count());
    }

    @Test
    public void clearTest() {
        projectEndpoint.clear();
        Assert.assertEquals(0, projectEndpoint.count());
    }

    @Test
    public void deleteByIdTest() {
        projectEndpoint.deleteById(project.getId());
        Assert.assertEquals(0, projectEndpoint.count());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(1, projectEndpoint.count());
    }

}
