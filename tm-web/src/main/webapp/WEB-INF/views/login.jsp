<jsp:include page="../include/_header.jsp"/>
<h1>LOGIN ON WEB</h1>

<form name="f" action='/auth' method='POST'>
    <table style="border-collapse: collapse; border: 1px solid black;"
           width="400" border="0" cellpadding="10" cellspacing="0" align="center">
        <tr>
            <td colspan="2">
                <h3>Login with Username and Password</h3>
            </td>
        </tr>
        <tr>
            <td>User:</td>
            <td><input type='text' name='username' value=''></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input type='password' name='password'></td>
        </tr>
        <tr>
            <td colspan='2'><input name="submit" type="submit" value="Login"/></td>
        </tr>
    </table>
</form>

<jsp:include page="../include/_footer.jsp"/>